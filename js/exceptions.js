/**
 * Created by yaroslav on 20.07.16.
 */
function NodeIdException(value) {
    this.value = value;
    this.message = " does not exist as id for DOM node!";
    this.toString = function() {
        return this.value + this.message;
    };
}