var MapTask = {
    center: new google.maps.LatLng({lat: 50.254661, lng: 28.658693}),
    markers: [],
    marker_active: null,
    marker_colors: {
        standard: '#00a651',
        active: '#90ff00'
    },
    zoom: 12,
    id: '',
    buttons: {
        add_id: '',
        add_info_id: '',
        delete_id: '',
        delete_info_id: '',
        directions_foot_id: '',
        directions_car_id: '',
        close_desc_id: ''
    },
    description_id: '',
    obj: null,
    directions: {
        service: null,
        display: null
    },
    init: function (id, description_id, buttons){
        var _this = this;
        this.id = id;
        this.description_id = description_id;
        // Init buttons for actions (with smart checking)
        if (typeof buttons === 'object') {
            for (var prop in buttons) {
                if (buttons.hasOwnProperty(prop)) {
                    buttons[prop] = {value: buttons[prop], writable: true};
                }
            }
            this.buttons = Object.defineProperties(this.buttons, buttons);
            for (prop in this.buttons) {
                if (this.buttons.hasOwnProperty(prop)) {
                    if (this.buttons[prop] == '' || document.getElementById(this.buttons[prop]) === null) {
                        throw new NodeIdException(this.buttons[prop]);
                    }
                }
            }
        }
        // Init google important objects
        this.obj = new google.maps.Map(document.getElementById(id), {
            center: this.center,
            zoom: this.zoom
        });
        this.directions.service = new google.maps.DirectionsService;
        this.directions.display = new google.maps.DirectionsRenderer({
            markerOptions: {
                visible: false
            }
        });
        this.directions.display.setMap(this.obj);
        this.directions.display.setPanel(document.getElementById('left-panel'));
        // Init google maps events
        google.maps.event.addListener(this.obj, 'center_changed', function () {
            _this.center = _this.obj.getCenter();
        });
        google.maps.event.addListener(this.obj, 'click', function () {
            _this.change_marker_inactive();
            _this.close_all_info_windows();
        });
        // Init DOM events
        document.getElementById(this.buttons.add_id).addEventListener('click', function () { _this.add_marker(); });
        document.getElementById(this.buttons.add_info_id).addEventListener('click', function () { _this.add_marker_info(); });
        document.getElementById(this.buttons.delete_id).addEventListener('click', function () { _this.delete_marker(); });
        document.getElementById(this.buttons.delete_info_id).addEventListener('click', function () { _this.delete_marker_info(); });
        document.getElementById(this.buttons.directions_foot_id).addEventListener('click', function () { _this.build_directions(google.maps.TravelMode.WALKING); });
        document.getElementById(this.buttons.directions_car_id).addEventListener('click', function () { _this.build_directions(google.maps.TravelMode.DRIVING); });
        document.getElementById(this.buttons.close_desc_id).addEventListener('click', function () { _this.close_description(); });
    },
    add_marker: function () {
        var marker_number = this.markers.length + 1, _this = this;
        var marker = new google.maps.Marker({
            position: this.center,
            draggable: true,
            map: this.obj,
            icon: this.generate_marker_image(marker_number)
        });
        this.markers.push({marker: marker, order: marker_number, info_window: null});
        google.maps.event.addListener(marker, 'rightclick', function () {
            _this.change_marker_order(_this.get_marker_index(marker) + 1);
        });
        google.maps.event.addListener(marker, 'click', function () {
            _this.change_marker_active(_this.get_marker_index(marker));
        });
    },
    add_marker_info: function () {
        if (this.isset_active_marker()) {
            var marker_data = this.get_marker_data(this.marker_active), _this = this;
            if (marker_data.info_window === null) {
                this.close_all_info_windows();
                var suffix = Math.random().toString(36).substring(7),
                    content_string = '<div id="info-window-' +
                        suffix +
                        '" class="marker-info-content">' +
                        '<div class="marker-info-body" style="display: none"></div>' +
                        '<textarea placeholder="Введіть тут бажаний текст для примітки" class="marker-info-edit"></textarea>' +
                        '<button class="marker-info-button map-button" data-type="save">Зберегти</button>' +
                        '</div>';
                var info_window = new google.maps.InfoWindow({
                    content: content_string,
                    maxWidth: 200,
                    block_id: 'info-window-' + suffix
                });
                var domready_listener = google.maps.event.addListener(info_window, 'domready', function () {
                    var button = document.querySelector('#' + info_window.block_id + ' > ' + '.marker-info-button');
                    button.addEventListener('click', function () {
                        _this.button_marker_info_action(button, marker_data);
                    });
                    google.maps.event.removeListener(domready_listener);
                });
                marker_data.info_window = info_window;
                var marker_active_listener = this.marker_active.addListener('click', function () {
                    if (marker_data.info_window === null) {
                        // hired, when info_window is removed after +1 click (closure effect)
                        google.maps.event.removeListener(marker_active_listener);
                    } else {
                        info_window.open(_this.obj, _this.marker_active);
                        _this.close_all_info_windows(info_window);
                    }
                });
                info_window.open(this.obj, this.marker_active);
            } else {
                alert('Цей маркер вже має вікно інформації!');
            }
        } else {
            this.alert_marker_not_selected();
        }
    },
    delete_marker: function () {
        if (this.isset_active_marker()) {
            var marker_data = this.get_marker_data(this.marker_active);
            var y = confirm('Ви впевнені, що хочете видалити маркер з порядковим номером ' + marker_data.order + ' ?');
            if (y) {
                this.marker_active.setMap(this.marker_active = null);
                this.markers.splice(this.markers.indexOf(marker_data), 1);
            }
        } else {
            this.alert_marker_not_selected();
        }
    },
    delete_marker_info: function () {
        if (this.isset_active_marker()) {
            var marker_data = this.get_marker_data(this.marker_active);
            if (marker_data.info_window !== null) {
                marker_data.info_window.setMap(marker_data.info_window = null);
            } else {
                alert('Цей маркер ще немає вікна для видалення!');
            }
        } else {
            this.alert_marker_not_selected();
        }
    },
    change_marker_order: function (number) {
        var cur_obj = this.markers[number - 1];
        var new_order = parseInt(prompt('Будь ласка, введіть новий порядоковий номер для цього маркера', cur_obj.order + ''));
        if (!isNaN(new_order)) {
            cur_obj.order = new_order;
            this.redraw_marker(cur_obj.marker, function (marker) {
                marker.icon = this.generate_marker_image(new_order, this.marker_active === cur_obj.marker ? this.marker_colors.active : undefined);
            });
        }
    },
    change_marker_active: function (marker_index) {
        if (this.marker_active !== this.markers[marker_index].marker) {
            if (this.isset_active_marker()) {
                this.close_all_info_windows();
                this.redraw_marker(this.marker_active, function (marker) {
                    marker.icon = this.generate_marker_image(this.get_marker_data(marker).order);
                });
            }
            this.redraw_marker(this.marker_active = this.markers[marker_index].marker, function (marker) {
                marker.icon = this.generate_marker_image(this.markers[marker_index].order, this.marker_colors.active);
            });
        }
    },
    change_marker_inactive: function () {
        if (this.isset_active_marker()) {
            this.redraw_marker(this.marker_active, function (marker) {
                marker.icon = this.generate_marker_image(this.get_marker_data(marker).order);
            });
            this.marker_active = null;
        }
    },
    build_directions: function (travel_mode) {
        if(this.markers.length >= 2) {
            if(this.normalize_markers_orders()) {
                var waypoints = [];
                // Redraw all markers with possible new orders and preparing them for waypoints
                for(var i = 0; i < this.markers.length; i++){
                    if(i > 0 && i < this.markers.length - 1){
                        waypoints.push({location: this.markers[i].marker.getPosition(), stopover: true});
                    }
                    this.redraw_marker(this.markers[i].marker, function (marker) {
                        marker.icon = this.generate_marker_image(this.get_marker_data(marker).order);
                    });
                }
                this.directions.service.route({
                    origin: this.markers[0].marker.getPosition(),
                    destination: this.markers[this.markers.length - 1].marker.getPosition(),
                    waypoints: waypoints,
                    travelMode: travel_mode
                }, function (response, status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                        this.directions.display.setDirections(response);
                        this.open_description();
                    } else {
                        alert('Запит на побудову (до гугл мап) впав: ' + status);
                    }
                }.bind(this));
            } else {
                alert('Ви маєте маркери із однаковими порядковими номерами, будь ласка, змініть їх!');
            }
        } else {
            alert('Для побудови маршруту має бути створено як мінімум 2 маркери (початок та кінець)!');
        }
    },
    normalize_markers_orders: function(){
        // TODO: Later make this process faster
        this.markers.sort(function (a, b) {
            return a.order - b.order;
        });
        // Checking
        for(var i = 0, has_similar_vals = false; i < this.markers.length - 1; i++){
            if(this.markers[i].order === this.markers[i + 1].order){
                has_similar_vals = true;
            }
        }
        if(!has_similar_vals){
            // Normalization
            for(i = 0; i < this.markers.length; i++){
                this.markers[i].order = i + 1;
            }
            return true;
        } else {
            return false;
        }
    },
    button_marker_info_action: function (button, marker_data) {
        var textarea = document.querySelector('#' + marker_data.info_window.block_id + ' > ' + '.marker-info-edit'),
            div_content = document.querySelector('#' + marker_data.info_window.block_id + ' > ' + '.marker-info-body');
        var status = button.getAttribute('data-type');
        switch (status) {
            case 'save':
                textarea.style.display = 'none';
                div_content.style.display = 'block';
                div_content.innerHTML = textarea.value.replace(/\r?\n/g, '<br>');
                button.setAttribute('data-type', 'edit');
                button.innerHTML = 'Редагувати';
                break;
            case 'edit':
                div_content.style.display = 'none';
                textarea.style.display = 'block';
                textarea.value = div_content.innerHTML.replace(/<br\s?\/?>/g, "\n");
                button.setAttribute('data-type', 'save');
                button.innerHTML = 'Зберегти';
                break;
        }
    },
    generate_marker_image: function (text, background_color, image_scale) {
        background_color = this.default_for(background_color, this.marker_colors.standard);
        image_scale = this.default_for(image_scale, 2);
        var canvas = document.createElement('canvas');
        canvas.width = 150 / image_scale;
        canvas.height = 205 / image_scale;
        var ctx = canvas.getContext('2d'), center_x = canvas.width / 2, scale = Math.sqrt(center_x * 7);
        ctx.beginPath(); // --Drawing marker body
        ctx.fillStyle = background_color;
        ctx.arc(center_x, center_x, center_x, 0, Math.PI, true);
        ctx.moveTo(0, center_x);
        ctx.bezierCurveTo(0, canvas.height, center_x, canvas.height, center_x, canvas.height);
        ctx.moveTo(canvas.width, center_x);
        ctx.bezierCurveTo(canvas.width, canvas.height, center_x, canvas.height, center_x, canvas.height);
        ctx.moveTo(0, center_x);
        ctx.lineTo(canvas.width, center_x);
        ctx.lineTo(center_x, canvas.height);
        ctx.fill();
        ctx.closePath();
        ctx.beginPath(); // --Drawing circle as a text background
        ctx.arc(center_x, center_x, center_x - scale, 0, 2 * Math.PI);
        ctx.clip();
        ctx.clearRect(0, 0, canvas.width, canvas.height); // slicing green background behind
        ctx.fillStyle = 'rgba(216,216,216,0.6)';
        ctx.fill();
        ctx.closePath();
        ctx.font = 'bold ' + scale + 'px cursive';
        ctx.textAlign = 'center';
        ctx.fillStyle = 'black';
        ctx.fillText(text, center_x, center_x); // --Drawing text there
        return canvas.toDataURL();
    },
    redraw_marker: function (marker, callback) {
        marker.setMap(null);
        if (typeof callback == 'function') {
            callback.call(this, marker);
        }
        marker.setMap(this.obj);
    },
    get_marker_data: function (marker) {
        return this.markers.find(function (element) {
            if (element.marker === marker) {
                return true;
            }
        });
    },
    get_marker_index: function (marker) {
        return this.markers.indexOf(this.get_marker_data(marker));
    },
    default_for: function (argument, value) {
        return typeof argument !== 'undefined' ? argument : value;
    },
    isset_active_marker: function () {
        return (this.marker_active !== null && typeof this.marker_active === 'object');
    },
    alert_marker_not_selected: function () {
        alert('Маркер не обрано! Натисність на будь-який маркер на карті аби обрати.');
    },
    close_all_info_windows: function (info_window) {
        // If info_window is defined close all windows without this one
        var is_undefined = typeof info_window === 'undefined';
        for (var i = 0; i < this.markers.length; i++) {
            var cur_info_window = this.markers[i].info_window;
            if (cur_info_window !== null && (is_undefined || (!is_undefined && cur_info_window !== info_window))) {
                cur_info_window.close();
            }
        }
    },
    open_description: function () {
        document.getElementById(this.description_id).style.display = 'block';
        document.getElementById(this.buttons.close_desc_id).style.display = 'block';
    },
    close_description: function () {
        document.getElementById(this.description_id).style.display = 'none';
        document.getElementById(this.buttons.close_desc_id).style.display = 'none';
    }
};

MapTask.init('map', 'left-panel', {
    add_id: 'add-marker',
    add_info_id: 'add-info-to-marker',
    delete_id: 'delete-marker',
    delete_info_id: 'delete-marker-info',
    directions_foot_id: 'build-road-for-foot',
    directions_car_id: 'build-road-for-car',
    close_desc_id: 'close-description'
});